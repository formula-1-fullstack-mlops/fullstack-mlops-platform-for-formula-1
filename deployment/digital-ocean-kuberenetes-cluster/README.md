# DigitalOcean Deployment
## Materials

- [Kubernets Dashboard](https://alexanderzeitler.com/articles/enabling-the-kubernetes-dashboard-for-digitalocean-kubernetes/)
- [NGINX Load Balancer and Cert Manager](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-with-cert-manager-on-digitalocean-kubernetes)